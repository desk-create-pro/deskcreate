## 添加客服授权使用合作，微W信X号：wxscrmplus

## [演示地址http://tag.deskcreate.com](http://tag.deskcreate.com/login?code=5)

#### 介绍
[DeskCreate]采用[配置式]开发模式,以低代码的开发方式,业务随需而变,配置自定义

### 效果图-使用系统开发好的应用客户端
![输入图片说明](https://foruda.gitee.com/images/1714449176577053563/41326237_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714449144310497864/3b3de89d_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714449193519022362/ded96d40_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714449215055246110/3621ad2d_8085813.png "屏幕截图")

### 效果图-使用系统开发应用
![输入图片说明](https://foruda.gitee.com/images/1714448833550634920/6193808a_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714448857266381377/185c5fb3_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714448872602978371/85ca14f1_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714448894015420979/6273e61b_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714448927795064779/4d4596b5_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714448974291989608/032cee7d_8085813.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1714448958816575682/c49dcd0b_8085813.png "屏幕截图")
